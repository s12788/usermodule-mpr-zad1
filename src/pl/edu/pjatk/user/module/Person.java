package pl.edu.pjatk.user.module;

import java.util.ArrayList;
import java.util.List;

public class Person {
	
	private String name;
	private String surname;
	private User user;
	private List <Address> address;
	private List <Role> role;
	
	public Person (){
		
		this.address = new ArrayList<Address>();
		this.role = new ArrayList<Role>();
	}

	public List<Role> getRole() {
		return role;
	}

	public void setRole(List<Role> role) {
		this.role = role;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Address> getAddress() {
		return address;
	}
	public void setAddress(List<Address> address) {
		this.address = address;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}

}
