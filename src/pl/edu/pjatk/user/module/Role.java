package pl.edu.pjatk.user.module;

import java.util.ArrayList;
import java.util.List;

public class Role {
	
	private String role;
	private Person person;
	private List <Permission> permission;
	
	public Role (){
		
		this.permission = new ArrayList<Permission>();
				
	}
	
	public List<Permission> getPermission() {
		return permission;
	}


	public void setPermission(List<Permission> permission) {
		this.permission = permission;
	}


	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	
	

}
